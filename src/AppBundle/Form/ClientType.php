<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\Client;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class, array('label' => 'Nom', 'attr' => array('class' => 'form-control')))
			->add('prenom', TextType::class, array('label' => 'Prenom', 'attr' => array('class' => 'form-control')))
			->add('tel', TextType::class, array('label' => 'Telephone', 'attr' => array('class' => 'form-control')))
			->add('email', TextType::class, array('label' => 'Email', 'attr' => array('class' => 'form-control')))
			->add('adresse',TextareaType::class,[
				'attr' => array('rows' => 4),
				'label' => 'Adresse', 
				'attr' => array('class' => 'form-control')
			])
			->add('type', EntityType::class, [
				// looks for choices from this entity
				'class' => 'AppBundle:TypeClient',

				// uses the User.username property as the visible option string
				'choice_label' => 'libelle',

				// used to render a select box, check boxes or radios
				'multiple' => false,
				// 'expanded' => true,
				'attr' => array('class' => 'form-control')
			]);
    }


	/**
	* @return string
	*/
	public function getName()
	{
		return 'appbundle_client';
	}
}