<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Client;

class DefaultController extends Controller
{
    public function indexAction(Request $request, $refresh = null)
    {
        $em = $this->getDoctrine()->getManager();
        $clients_liste = $em->getRepository('AppBundle:Client')->findAll();
		$count = $em->getRepository('AppBundle:Client')->getCount();
		
		$paginator = $this->get('knp_paginator');
        $clients = $paginator->paginate($clients_liste, $request->query->getInt('page', 1), 2);
		
		if($refresh == null)
			return $this->render('AppBundle:Default:index.html.twig',['clients' => $clients, 'count' => $count]);
		else
			return $this->render('AppBundle:Default:refresh.html.twig',['clients' => $clients, 'count' => $count]);
    }

    public function operationAction($id = null, Request $request)
    {       
		if($id != null) {
			$em = $this->getDoctrine()->getManager();
			$client = $em->getRepository('AppBundle:Client')->find($id);
			if(!$client)
				throw $this->createNotFoundException('Client introuvable');
		}
		else {
			 $client = new Client();
		}
       
        $form = $this->createForm('AppBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();
			
			$this->addFlash(
				'notice',
				'Opération effectué avec succès'
			);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('AppBundle:Default:add.html.twig', array(
            'client' => $client,
            'form' => $form->createView(),
        ));
    }
	
    public function deleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:Client')->find($id);
        if(!$client)
            throw $this->createNotFoundException('Client introuvable');

        $em->remove($client);
        $em->flush();
        
        $this->addFlash(
            'notice',
            'Client supprimé avec succès'
        );

        return $this->redirectToRoute('homepage');
    }
	
}
